package millionaire.ave

import grails.test.mixin.TestFor
import com.emathias.millionaireave.BankListing
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(BankListing)
class BankListingSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
