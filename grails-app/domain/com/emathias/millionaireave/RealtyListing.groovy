package com.emathias.millionaireave

class RealtyListing {

    User user;
    MyProperty myProperty;
    int askingPrice;
    int offeredPrice;
    boolean offerAccepted;

    static belongsTo = [User, MyProperty]

    static constraints = {
    }
}
