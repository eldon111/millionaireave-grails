package com.emathias.millionaireave

import org.springframework.security.core.userdetails.UserDetails

class User implements UserDetails {

	transient springSecurityService

    String username
	String password
    String firstname
    String lastname
    String nickname
    boolean accountExpired
    boolean accountLocked
    boolean credentialsExpired
    boolean enabled = true
    int cash
    int turn

    // starting age in months
    static final int STARTING_AGE = (25 * 12)

    static hasMany = [roles: Role, openIds: OpenID]

    static transients = ['STARTING_AGE']

    static constraints = {
		username blank: false, unique: true
		password nullable: true, blank: false
        nickname nullable: true
    }

	static mapping = {
		password column: '`password`'
	}

	User() {}

	User(String username, String firstname, String lastname) {
		this.username = username
		this.firstname = firstname
		this.lastname = lastname
	}

    int getAge() {
        return turn + STARTING_AGE
    }

	void addRole(Role role) {
		roles.add(role)
	}

	@Override
	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
//		List<String> roles = new ArrayList<>(roles.size());
//		for (Role role : this.roles) {
//			roles.add(role.role)
//		}
//		return AuthorityUtils.createAuthorityList(roles.toArray(new String[roles.size()]))
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		if (password) {
			password = springSecurityService.encodePassword(password)
		}
	}

	@Override
	String getPassword() {
		return null
	}

	@Override
	boolean isAccountNonExpired() {
		return !accountExpired
	}

	@Override
	boolean isAccountNonLocked() {
		return !accountLocked
	}

	@Override
	boolean isCredentialsNonExpired() {
		return !credentialsExpired
	}

	String getFullName() {
		firstname + " " + lastname
	}
}
