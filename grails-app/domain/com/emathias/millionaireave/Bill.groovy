package com.emathias.millionaireave

class Bill {

    User user;
    String name;
    Integer monthlyPayment;
    BillType billType;
    Importance importance;

    static belongsTo = [User]

    static constraints = {
    }
}
