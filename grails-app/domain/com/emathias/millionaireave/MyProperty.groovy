package com.emathias.millionaireave

class MyProperty {

    User user;
    Loan mortgage;
    String name;
    Integer numOfUnits;
    Integer value;
    Quality quality;
    PropertyType propertyType;

    static belongsTo = [User]
    static hasMany = [renters: Renter]

    static constraints = {
        mortgage nullable: true
        user nullable: true
    }

    public int getRentalIncome() {
//		int total = 0
//		for (Renter renter : renters) {
//			total += renter.monthlyPayment
//		}
        renters.sum {it.monthlyPayment} as Integer ?: 0
//		return total
    }

    int getTotalIncome() {
        rentalIncome - mortgagePayment
//        if (mortgage != null) {
//            return rentalIncome - mortgage.monthlyPayment;
//        } else {
//            return rentalIncome;
//        }
    }

    int getMortgagePayment() {
        mortgage?.monthlyPayment ?: 0
//        if (getMortgage() != null) {
//            return getMortgage().getMonthlyPayment();
//        } else {
//            return 0;
//        }
    }
}
