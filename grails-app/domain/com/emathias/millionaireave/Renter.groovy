package com.emathias.millionaireave

class Renter {

    MyProperty myProperty;
    String name;
    Integer monthlyPayment;
    Integer monthsLeftOnLease;
    Float reliability;

    static belongsTo = [MyProperty]

    static constraints = {
		myProperty nullable: true
    }
}
