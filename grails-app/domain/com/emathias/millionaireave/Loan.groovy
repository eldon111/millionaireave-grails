package com.emathias.millionaireave

class Loan {

    User user;
    String name;
    Integer balance;
    Integer downPayment;
    Integer monthlyPayment;
    LoanType loanType;
    Importance importance;

    static belongsTo = [User]

    static constraints = {
        user nullable: true
    }
}
