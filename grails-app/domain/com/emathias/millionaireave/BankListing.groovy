package com.emathias.millionaireave

class BankListing {

    User user
    MyProperty myProperty
    static belongsTo = [MyProperty, User];

    static constraints = {
        myProperty unique: true
    }
}
