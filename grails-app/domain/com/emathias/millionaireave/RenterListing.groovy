package com.emathias.millionaireave

class RenterListing {

    MyProperty myProperty;
    Renter renter;

    static belongsTo = [MyProperty, Renter]

    static constraints = {
    }
}
