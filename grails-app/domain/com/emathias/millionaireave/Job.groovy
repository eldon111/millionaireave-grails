package com.emathias.millionaireave

class Job {

	User user;
    String title;
	Integer monthlySalary;
	Integer hoursPerWeek;
	Float jobSecurity;

    static belongsTo = [User]

    static constraints = {
		user nullable: true
    }
}
