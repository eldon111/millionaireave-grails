package com.emathias.millionaireave

class JobListing {

    User user;
    Job job;

    static belongsTo = [User, Job]

    static constraints = {
        job unique: true, nullable: false
		user nullable: false
    }
}
