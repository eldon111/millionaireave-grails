<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <g:render template="jobMenuTemplate" model="[findJobsActive: true]" />
    <div class="well well-sm">
        <g:render template="availableJobSummaryTemplate" var="jobListing" collection="${listings}" />
    </div>
</body>
</html>