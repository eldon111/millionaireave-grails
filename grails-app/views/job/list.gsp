<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <g:render template="jobMenuTemplate" model="[myJobsActive: true]" />
    <div class="well well-sm">
        <g:render template="heldJobSummaryTemplate" var="job" collection="${jobs}" />
    </div>
</body>
</html>