<%@ page import="com.emathias.millionaireave.FormatUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <div class="row">
        <div class="col-xs-12">
            <h4>Job Information</h4>
        </div>
    </div>

    <g:if test="${job}">
    <div class="row">
        <div class="col-xs-12">
            <h5 class="bold">${job.title}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <dl class="dl-horizontal">
                <dt>Hours</dt>
                <dd class="text-right">${job.hoursPerWeek}</dd>
                <dt>Monthly Salary</dt>
                <dd class="text-right">${job.monthlySalary}</dd>
            </dl>
        </div>
    </div>
    </g:if>
</body>
</html>