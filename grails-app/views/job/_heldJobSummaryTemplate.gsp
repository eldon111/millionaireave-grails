<%@ page import="com.emathias.millionaireave.FormatUtils" %>
<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="bold"><g:link action="show" id="${job.id}">${job.title}</g:link></div>
    </div>
    <div class="col-sm-2 col-xs-6">
        <div class="bold">Hours: ${job.hoursPerWeek}</div>
    </div>
    <div class="col-sm-4 col-xs-6">
        <div class="bold text-right">Monthly Salary: ${FormatUtils.currencyString(job.monthlySalary)}</div>
    </div>
</div>