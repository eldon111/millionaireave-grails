<%@ page import="com.emathias.millionaireave.FormatUtils" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Millionaire Ave"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'jquery-2.1.0.min.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'application.js')}"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css"/>
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-theme.min.css')}" type="text/css"/>--}%
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'application.css')}" type="text/css"/>
    <g:javascript library="application"/>
    <g:layoutHead/>
    <r:layoutResources />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 pull-right">
                        <g:if test="${hideLogin}"> <%--or ${#authorization.expression('hasRole(''ROLE_PERSIST'')')}--%>
                            <r:script>
                                $(document).ready(function () {
                                    $("#linkSignin").click(function () {
                                        $("#openid_form").submit();
                                    });

                                    $("#btnEndTurn").click(function () {
                                        return confirm("Are you sure you want to end this turn?");
                                    });
                                });
                            </r:script>
                            <g:form id="openid_form" controller="login" action="openid" method="post">
                                <fieldset>
                                    <input id="openid_identifier" name="openid_identifier" type="hidden"
                                           value="https://www.google.com/accounts/o8/id"/>
                                    <a href="#" id="linkSignin" class="pull-right">Sign in with Google</a>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </fieldset>
                            </g:form>
                        </g:if>
                        <div>
                            <a href="/endturn" id="btnEndTurn" class="btn btn-primary btn-sm pull-right"
                               style="margin:5px;">End Turn</a>
                        </div>
                    </div>
                    <h1 class="hidden-xs col-sm-7 pull-left clickable link">
                        Millionaire Ave
                        <a style="display:none;" class="target-link" href="/">Home Page</a>
                    </h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="well well-sm">
                    <div class="bold text-center">${playerName}</div>
                    <table class="bold table-info">
                        <tr>
                            <td>Age:</td>
                            <td class="text-right">${playerAge}</td>
                        </tr>
                        <tr>
                            <td>Cash:</td>
                            <td class="text-right">${FormatUtils.currencyString(playerCash)}</td>
                        </tr>
                        <tr>
                            <td>Cashflow:</td>
                            <td class="text-right">${FormatUtils.signedCurrencyString(playerCashflow)}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9">
                %{--<div class="well well-sm">--}%
                    <g:layoutBody/>
                %{--</div>--}%
            </div>
        </div>
    </div>
    <r:layoutResources />
</body>
</html>