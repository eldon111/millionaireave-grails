<%@ page import="com.emathias.millionaireave.FormatUtils" %>
<div class="row">
    <div class="col-sm-8 col-xs-12">
        <div class="bold"><g:link action="buy" id="${listing.id}">${listing.myProperty.quality.name} ${listing.myProperty.name}</g:link></div>
    </div>
    <div class="col-sm-2 col-xs-6">
        <div class="bold">Units: ${listing.myProperty.numOfUnits}</div>
    </div>
    <div class="col-sm-2 col-xs-6">
        <div class="bold text-right">${FormatUtils.currencyString(listing.myProperty.value)}</div>
    </div>
</div>