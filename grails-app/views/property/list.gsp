<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <g:render template="propertyMenuTemplate" model="[myPropertiesActive: true]" />
    <div class="well well-sm">
        <g:render template="ownedPropertySummaryTemplate" var="property" collection="${properties}" />
    </div>
</body>
</html>