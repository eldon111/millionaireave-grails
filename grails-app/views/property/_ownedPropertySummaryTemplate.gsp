<%@ page import="com.emathias.millionaireave.FormatUtils" %>
<div class="row">
    <div class="col-sm-8 col-xs-12">
        <div class="bold"><g:link action="show" id="${property.id}">${property.quality.name} ${property.name}</g:link></div>
    </div>
    <div class="col-sm-2 col-xs-6">
        <div class="bold">Units: ${property.numOfUnits}</div>
    </div>
    <div class="col-sm-2 col-xs-6">
        <div class="bold text-right">${FormatUtils.currencyString(property.value)}</div>
    </div>
</div>