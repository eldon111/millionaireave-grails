<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <g:render template="propertyMenuTemplate" model="[buyPropertiesActive: true]" />
    <div class="well well-sm">
        <g:render template="availablePropertySummaryTemplate" var="listing" collection="${listings}" />
    </div>
</body>
</html>