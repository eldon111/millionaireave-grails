<%@ page import="com.emathias.millionaireave.FormatUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <div class="row">
        <g:each in="${renterListings}" var="renterListing">
            <div class="col-xs-12">
                <g:link action="lease" id="${renterListing.id}">${renterListing.renter.name}:</g:link>
                ${FormatUtils.currencyString(renterListing.renter.monthlyPayment)}
            </div>
        </g:each>
    </div>
</body>
</html>