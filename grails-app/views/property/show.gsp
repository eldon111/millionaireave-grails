<%@ page import="com.emathias.millionaireave.FormatUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
    <div class="row">
        <div class="col-xs-12">
            <h4>Property Information</h4>
        </div>
    </div>

    <g:if test="${property}">
    <div class="row">
        <div class="col-xs-12">
            <h5 class="bold">${property.name}</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <dl class="dl-horizontal">
                <dt>Type</dt>
                <dd class="text-right">${property.propertyType.name}</dd>
                <dt>Quality</dt>
                <dd class="text-right">${property.quality.name}</dd>
                <dt>Renters / Units</dt>
                <dd class="text-right">${property.renters.size()} / ${property.numOfUnits}</dd>
                <dt>Value</dt>
                <dd class="text-right">${FormatUtils.currencyString(property.value)}</dd>
                <dt>Mortgage payment</dt>
                <dd class="text-right">${FormatUtils.currencyString(property.mortgagePayment)}</dd>
                <dt>Rental Income</dt>
                <dd class="text-right">${FormatUtils.currencyString(property.rentalIncome)}</dd>
            </dl>
        </div>
    </div>
    <div class="bold">
        <g:link action="renters" id="${property.id}">Find Renters</g:link>
    </div>
    </g:if>
</body>
</html>