<%@ page import="com.emathias.millionaireave.FormatUtils" %>
<!DOCTYPE html>
<html>
	<head></head>
	<body>
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-12">
                    <strong><g:link controller="property" action="list">Real Estate</g:link></strong>
                    <span class="pull-right bold">Monthly: ${FormatUtils.signedCurrencyString(rentalIncome)}</span>
                </div>
                <div class="col-xs-5 col-sm-3">
                    <table class="table-info">
                        <tr>
                            <td>Properties:</td>
                            <td>${numOfProperties}</td>
                        </tr>
                        <tr>
                            <td>Renters:</td>
                            <td>${numOfRenters} / ${maxNumOfRenters}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-12">
                    <strong><g:link controller="job" action="list">Jobs</g:link></strong>
                    <span class="pull-right bold">Monthly: ${FormatUtils.signedCurrencyString(jobIncome)}</span>
                </div>
                <div class="col-xs-5 col-sm-3">
                    <table class="table-info">
                        <g:each in="${jobSummaries}" var="summary">
                            <tr>
                                <td>${summary}</td>
                            </tr>
                        </g:each>
                    </table>
                </div>
            </div>
        </div>
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-12">
                    <strong><g:link controller="property" action="list">Loans</g:link></strong>
                    <span class="pull-right bold">Monthly: ${FormatUtils.signedCurrencyString(loanOutgo)}</span>
                </div>
                <div class="col-xs-5 col-sm-3">
                    <table class="table-info">
                        <tr>
                            <td>Balance:</td>
                            <td>${FormatUtils.currencyString(loansBalance)}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-12">
                    <strong><g:link controller="property" action="list">Bills</g:link></strong>
                    <span class="pull-right bold">Monthly: ${FormatUtils.signedCurrencyString(billOutgo)}</span>
                </div>
            </div>
        </div>
	</body>
</html>
