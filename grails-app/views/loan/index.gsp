<%@ page import="com.emathias.millionaireave.FormatUtils" contentType="text/html;charset=UTF-8" %>
<html>
<body>
<g:each in="loans" var="loan">
    <div class="bold well well-sm">
        <div class="col-xs-6">
            ${loan.name}:
        </div>
        <div class="col-xs-6 pull-right">
            ${FormatUtils.currencyString(loan.balance)}
        </div>
    </div>
</g:each>
</body>
</html>