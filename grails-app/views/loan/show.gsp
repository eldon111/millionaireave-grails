<%@ page import="com.emathias.millionaireave.Loan" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'loan.label', default: 'Loan')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-loan" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-loan" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list loan">

        <g:if test="${loanInstance?.balance}">
            <li class="fieldcontain">
                <span id="balance-label" class="property-label"><g:message code="loan.balance.label"
                                                                           default="Balance"/></span>

                <span class="property-value" aria-labelledby="balance-label"><g:fieldValue bean="${loanInstance}"
                                                                                           field="balance"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.downPayment}">
            <li class="fieldcontain">
                <span id="downPayment-label" class="property-label"><g:message code="loan.downPayment.label"
                                                                               default="Down Payment"/></span>

                <span class="property-value" aria-labelledby="downPayment-label"><g:fieldValue bean="${loanInstance}"
                                                                                               field="downPayment"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.importance}">
            <li class="fieldcontain">
                <span id="importance-label" class="property-label"><g:message code="loan.importance.label"
                                                                              default="Importance"/></span>

                <span class="property-value" aria-labelledby="importance-label"><g:fieldValue bean="${loanInstance}"
                                                                                              field="importance"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.loanType}">
            <li class="fieldcontain">
                <span id="loanType-label" class="property-label"><g:message code="loan.loanType.label"
                                                                            default="Loan Type"/></span>

                <span class="property-value" aria-labelledby="loanType-label"><g:fieldValue bean="${loanInstance}"
                                                                                            field="loanType"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.monthlyPayment}">
            <li class="fieldcontain">
                <span id="monthlyPayment-label" class="property-label"><g:message code="loan.monthlyPayment.label"
                                                                                  default="Monthly Payment"/></span>

                <span class="property-value" aria-labelledby="monthlyPayment-label"><g:fieldValue bean="${loanInstance}"
                                                                                                  field="monthlyPayment"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="loan.name.label" default="Name"/></span>

                <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${loanInstance}"
                                                                                        field="name"/></span>

            </li>
        </g:if>

        <g:if test="${loanInstance?.user}">
            <li class="fieldcontain">
                <span id="user-label" class="property-label"><g:message code="loan.user.label" default="User"/></span>

                <span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show"
                                                                                  id="${loanInstance?.user?.id}">${loanInstance?.user?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: loanInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${loanInstance}"><g:message code="default.button.edit.label"
                                                                                     default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
