package com.emathias.millionaireave

class LoanController {

    LoanService loanService

    def index() {
        ['loans': loanService.loans]
    }

    def show() {
        ['loan': loanService.getLoan(params.long('id'))]
    }

    def apply() {
        int amount = params.int('amount')
        if (amount) {
            loanService.applyForLoan(amount, loanService.user)
        }

        // TODO: no view, only data response
        redirect(action: 'index')
    }
}
