package com.emathias.millionaireave

class PropertyController {

    PropertyService propertyService
    RenterService renterService

	def index() {
		redirect(action: "list")
	}

    def list() {
        MyProperty[] ownedProperties = propertyService.ownedProperties
        def rentersByProperty = [:]
        ownedProperties.each {
            rentersByProperty[it.id] = propertyService.getRentersInProperty(it)
        }

        ['properties':ownedProperties, 'rentersByProperty':rentersByProperty]
    }

    def show() {
		['property':propertyService.getOwnedProperty(params.long('id')), 'renters':propertyService.renters]
    }

    def renters() {
        // TODO: remove after testing
        renterService.refreshRenterListings();

        long propertyId = params.long('id');
        ['renterListings':renterService.getRenterListings(propertyId), 'property':propertyService.getOwnedProperty(propertyId)]
    }

	def available() {
		// TODO: remove after testing
		propertyService.refreshAvailableProperties()

		[listings: propertyService.realtyListings]
	}

    def buy() {
        propertyService.purchaseProperty(params.long("id"), true)
        redirect(action: "list")
    }

    def lease() {
        propertyService.lease(params.long("id"))
        redirect(action: "list")
    }
}
