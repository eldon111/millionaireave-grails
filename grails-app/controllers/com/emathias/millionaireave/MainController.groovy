package com.emathias.millionaireave

class MainController {

    PropertyService propertyService
    LoanService loanService
    BillService billService
    JobService jobService
	UserService userService

    def index() {
        def model = [:]

        // myProperty info
        model['rentalIncome'] = propertyService.rentalIncome
        model['numOfProperties'] = propertyService.numOfProperties
        model['numOfRenters'] = propertyService.numOfRenters
        model['maxNumOfRenters'] = propertyService.maxNumOfRenters

        // job info
        model['jobIncome'] = jobService.jobIncome
        model['jobSummaries'] = jobService.jobSummaries

        // loan info
        model['loanOutgo'] = loanService.loanOutgo
        model['loansBalance'] = loanService.loansBalance

        // bill info
        model['billOutgo'] = billService.billOutgo

        return model
    }

	def endturn() {
		userService.newTurn()
		redirect(action: "index")
	}
}
