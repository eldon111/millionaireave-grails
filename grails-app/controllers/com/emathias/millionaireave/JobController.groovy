package com.emathias.millionaireave

class JobController {

	def JobService jobService

    def index() {
		redirect(action: "list")
	}

	def list() {
		['jobs': jobService.getHeldJobs(jobService.user)]
	}

	def show() {
		['job':jobService.getHeldJob(params.long('id'))]
	}

	def available() {
		//TODO: remove after testing
		jobService.refreshJobListings(jobService.user)

		[listings: jobService.getJobListings(jobService.user)]
	}

	def apply() {
		jobService.applyForJob(params.long("id"))
		redirect(action: "list")
	}
}
