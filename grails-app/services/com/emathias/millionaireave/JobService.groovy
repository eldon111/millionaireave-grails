package com.emathias.millionaireave

import grails.transaction.Transactional

@Transactional
class JobService extends BaseService implements TurnSensitive {

    private static final int TARGET_JOB_LISTINGS = 4;
    private static final int MAX_LISTING_VARIANCE = 1;
    private static final double REFRESH_CHANCE = 0.3;
    private static final int MAX_WEEKLY_HOURS = 80;
    private static final int MAX_JOBS = 5;
    private final Random random = new Random();

    List<Job> getHeldJobs(User user) {
        Job.findAllByUser(user)
    }

	public Job getHeldJob(long jobId) {
		Job.findByIdAndUser(jobId)
	}

    int getJobIncome(User user) {
        int total = 0;
        getHeldJobs(user).each {
            total += it.getMonthlySalary();
        }
        return total;
    }

    int getJobHours(User user) {
        int total = 0;
        getHeldJobs(user).each {
            total += it.getHoursPerWeek();
        }
        return total;
    }

    List<String> getJobSummaries(User user) {
        List<String> jobSummaries = new ArrayList<>();
        getHeldJobs(user).each {
            jobSummaries.add(String.format("%s:\t%dhrs.\t%s",
                    it.getTitle(), it.getHoursPerWeek(), FormatUtils.currencyString(it.getMonthlySalary())));
        }
        return jobSummaries;
    }

    List<JobListing> getJobListings(User user) {
        JobListing.findAllByUser(user)
    }

    private static Job generateRandomJob() {
		Job job = new Job()
		job.title = "title"
		job.monthlySalary = 500
		job.hoursPerWeek = 40
		job.jobSecurity = 0.5f
		job.save()
		return job
    }

    void addJob(Job job) {
        if (job.user) {
            job.save()
        }
    }

    void applyForJob(long listingId) {
        JobListing jobListing = JobListing.get(listingId)
		if (jobListing == null) {
			return
		}
		User user = jobListing.user
		if (getJobHours(user) >= MAX_WEEKLY_HOURS) {
            return
        }
        if (getHeldJobs(user).size() >= MAX_JOBS) {
            return
        }
        jobListing.job.setUser(user)
        jobListing.job.save()
        jobListing.delete()
    }

    private static void replaceRandomJobs(List<JobListing> jobListings) {
        jobListings.each {
            if (Math.random() < REFRESH_CHANCE) {
                it.properties = generateRandomJob()
                //it.getJob().mapNewValues(generateRandomJob());
            }
        }
    }

    private void ensureTargetJobCount(List<JobListing> jobListings) {
        int variance = random.nextInt(MAX_LISTING_VARIANCE) + 1;
        variance *= (random.nextBoolean() ? -1 : 1);
        int newTarget = TARGET_JOB_LISTINGS + variance;

        while (jobListings.size() != newTarget) {
            if (jobListings.size() < newTarget) {
                Job job = generateRandomJob();
                jobListings.add(new JobListing(user:getUser(), job:job));
            } else {
                jobListings.remove(0);
            }
        }
    }

    /**
     * Randomly replaces some jobs, and also may change the number of
     * jobs available
     */
    void refreshJobListings(User user) {
        List<JobListing> jobListings = getJobListings(user);
        replaceRandomJobs(jobListings);
        ensureTargetJobCount(jobListings);
        JobListing.saveAll(jobListings)
    }

    @Override
    void newTurn(User user) {
        refreshJobListings(user);
    }
}
