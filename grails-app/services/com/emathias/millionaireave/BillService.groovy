package com.emathias.millionaireave

import grails.transaction.Transactional

@Transactional
class BillService extends BaseService {

    List<Bill> getBills(User user) {
        Bill.findAllByUser(user)
    }

    int getBillOutgo(User user) {
        int total = 0;
        getBills(user).each {
            total += it.getMonthlyPayment();
        }
        return total;
    }

	void payAllBills(User user) {
		Bill.findAllByUser(user).each {
			user.cash -= it.monthlyPayment
			user.save()
		}
	}
}
