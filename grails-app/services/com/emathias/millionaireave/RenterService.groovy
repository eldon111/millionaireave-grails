package com.emathias.millionaireave

import com.emathias.millionaireave.exceptions.InvalidPropertyException
import grails.transaction.Transactional

@Transactional
class RenterService extends BaseService implements TurnSensitive {

    PropertyService propertyService;

    private static final int TARGET_RENTER_LISTINGS = 3;
    private static final int MAX_LISTING_VARIANCE = 1;
    private static final double REFRESH_CHANCE = 0.7;
    private final Random random = new Random();

    List<RenterListing> getRenterListings(long propertyId) {
        RenterListing.createCriteria().list {
            eq('myProperty.id', propertyId)
        }
    }

    def leaseToRenter(String renterListingId) throws InvalidPropertyException {
//        List<MyProperty> properties = propertyService.ownedProperties

        RenterListing renterListing = RenterListing.get(renterListingId);
		List<Renter> renters = renterListing.myProperty.renters
//        List<Renter> renters = Renter.findAllByMyProperty(renterListing.myProperty);

//		for (MyProperty myProperty : properties) {
//			for (RenterListing renterListing : getRenterListings(myProperty.getId())) {
//				if (renterListing.getId().equals(renterListingId)) {
        if (renters.size() >= renterListing.myProperty.numOfUnits) {
            throw new InvalidPropertyException("This property already has enough renters!");
        }

//		renters.add(renterListing.getRenter());
        renterListing.renter.setMyProperty(renterListing.myProperty)
        renterListing.renter.save()
        renterListing.delete()

//		renterListing.getProperty().getRenters().add(renterListing.getRenter());
//		renterListing.getProperty().getRenterListings().remove(renterListing);

//		dataAccessService.updateProperties(properties);
//		return;
//				}
//			}
//		}
    }

    private static void replaceRandomRenters(List<RenterListing> renterListings) {
        renterListings.each {
            if (Math.random() < REFRESH_CHANCE) {
                it.renter.properties = generateRandomRenterProperties();
				it.save()
            }
        }
    }

    private void ensureTargetRenterCount(List<RenterListing> renterListings, MyProperty property) {
        int variance = random.nextInt(MAX_LISTING_VARIANCE) + 1;
        variance *= (random.nextBoolean() ? -1 : 1);
        int newTarget = TARGET_RENTER_LISTINGS + variance;

        while (renterListings.size() != newTarget) {
            if (renterListings.size() < newTarget) {
                Renter renter = new Renter(generateRandomRenterProperties()).save()
				RenterListing renterListing = new RenterListing(myProperty: property, renter: renter)
                renterListings.add(renterListing);
            } else {
                renterListings.remove(0).delete();
            }
        }
    }

    private static Map generateRandomRenterProperties() {
        // TODO: real/better random values
        Renter renter = new Renter(name:"Renter Name", monthlyPayment: 200, monthsLeftOnLease: 12, reliability: 0.5);
        return renter.properties;
    }

    /**
     * Randomly replaces some properties, and also may change the number of
     * properties available
     */
    public void refreshRenterListings(User user) {
        propertyService.getOwnedProperties(user).each {
            List<RenterListing> renterListings = getRenterListings(it.id);
            replaceRandomRenters(renterListings);
            ensureTargetRenterCount(renterListings, it);
            RenterListing.saveAll(renterListings)
        }
    }

    @Override
    public void newTurn(User user) {
        refreshRenterListings(user);
    }
}
