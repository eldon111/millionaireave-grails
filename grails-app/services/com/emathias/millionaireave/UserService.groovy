package com.emathias.millionaireave

import grails.transaction.Transactional

@Transactional
class UserService extends BaseService implements TurnSensitive {

    JobService jobService
    PropertyService propertyService
    BillService billService
    LoanService loanService

    String getPlayerFullName(User user) {
        return "${user.firstname} ${user.lastname}";
    }

    public int getCash(User user) {
        return user.cash;
    }

    public int getMonthlyCashflow(User user) {
        return monthlyIncome - getMonthlyOutgo(user)
    }

    public int getMonthlyIncome(User user) {
        return jobService.jobIncome + propertyService.rentalIncome;
    }

    public int getMonthlyOutgo(User user) {
        return billService.getBillOutgo(user) + loanService.getLoanOutgo(user);
    }

    public String getAgeString(User user) {
        int age = user.age;
        int years = age / 12;
        int months = age - (years * 12);
        return "${years}y ${months}m"
    }

    @Override
    public void newTurn(User user) {
        user.turn++;
        //user.cash += monthlyCashflow;
		billService.payAllBills(user)
		loanService.makeAllMonthlyLoanPayments(user);
        user.save()
    }
}
