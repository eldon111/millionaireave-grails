package com.emathias.millionaireave

import org.springframework.security.core.userdetails.AuthenticationUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.openid.OpenIDAuthenticationToken

public class OpenIdUserDetailsService
		implements UserDetailsService, AuthenticationUserDetailsService<OpenIDAuthenticationToken> {

	UserService userService

	@Override
	public UserDetails loadUserDetails(OpenIDAuthenticationToken token) throws UsernameNotFoundException {
		String email = getEmail(token)
		try {
			return loadUserByUsername(email)
		} catch (UsernameNotFoundException e) {
			String firstname = getFirstname(token)
			String lastname = getLastname(token)
			User user = new User(email, firstname, lastname)
			Role role = getRole(Roles.PERSISTED_USER)
			if (!role) {
				role = new Role(authority: Roles.PERSISTED_USER).save()
			}
//			user.addRole(new Role(user, Roles.PERSISTED_USER))
			userService.initializeUser(user)
			user.save()
			UserRole.create(user, role, true)
			return user
		}
	}

	Role getRole(String authority) {
		return Role.findByAuthority(authority)
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = User.get(username);
		if (user == null) {
			throw new UsernameNotFoundException("User does not exist in database");
		}
		return user;
	}

	private static String getEmail(OpenIDAuthenticationToken token) {
		token.attributes.each {
			if (it.name == "email") {
				return it.values[0];
			}
		}
		return null;
	}

	private static String getFirstname(OpenIDAuthenticationToken token) {
		token.attributes.each {
			if (it.name == "firstname") {
				return it.values[0];
			}
		}
		return null;
	}

	private static String getLastname(OpenIDAuthenticationToken token) {
		token.attributes.each {
			if (it.name == "lastname") {
				return it.values[0];
			}
		}
		return null;
	}
}