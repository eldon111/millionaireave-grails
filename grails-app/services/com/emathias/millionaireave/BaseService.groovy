package com.emathias.millionaireave

import grails.plugin.cache.Cacheable
import org.apache.commons.lang.RandomStringUtils
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.openid.OpenIDAuthenticationToken
import org.springframework.stereotype.Service

@Service
public abstract class BaseService {

	@Cacheable("user")
    public User getUser() {
		try {
            User u = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()
            return User.get(u.id)
		} catch (e) {
			User user = createAnonymousUser(RandomStringUtils.randomAlphanumeric(20)).save(flush: true);
			OpenIDAuthenticationToken token = new OpenIDAuthenticationToken(user, user.authorities, "url", null);
			SecurityContextHolder.getContext().setAuthentication(token);
			return user;
		}
    }

    public User getUser(String username) {
        return User.get(username);
    }

    public void saveUser(User user) {
        user.save(flush: true);
    }

    public void initializeUser(User user) {
        user.setCash(10000);
        user.setTurn(0);
    }

    public User createAnonymousUser(String username) {
        User user = new User()
        user.setUsername(username)
        user.setFirstname("First")
        user.setLastname("Last")
        initializeUser(user)
		Role role = Role.findByAuthority(Roles.ANONYMOUS_USER)
		if (!role) {
			role = new Role(authority: Roles.ANONYMOUS_USER).save()
		}
		user.save()
		new UserRole(user: user, role: role).save()
//        user.roles = [new Role(authority: Roles.ANONYMOUS_USER)]
//        user.roles.add(new Role(role: Roles.ANONYMOUS_USER));
        return user;
    }
}
