package com.emathias.millionaireave

import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils
import org.apache.commons.lang.math.RandomUtils

@Transactional
class PropertyService extends BaseService implements TurnSensitive {

	double REFRESH_CHANCE = 0.3
	int MAX_LISTING_VARIANCE = 1
	int TARGET_PROPERTY_LISTINGS = 8

	Random random = new Random()

    public List<MyProperty> getOwnedProperties(User user) {
        MyProperty.findAllByUser(user)
    }

    public MyProperty getOwnedProperty(long propertyId, User user) {
        MyProperty.findByIdAndUser(propertyId, user)
    }

	public List<MyProperty> getAvailableProperties(User user) {
		getRealtyListings(user).collect { it.myProperty };
	}

    public void purchaseProperty(long listingId, boolean cashPurchase) {
        RealtyListing realtyListing = RealtyListing.get(listingId)
		User user = realtyListing.user
        Loan mortgage
        if (!cashPurchase) {
            mortgage = generateMortgageForProperty(realtyListing.myProperty)
            mortgage.user = user
            if (user.cash < mortgage.downPayment) {
                return
            }
            user.cash -= mortgage.downPayment
            realtyListing.myProperty.mortgage = mortgage
            mortgage.save()
        } else {
            if (user.cash < realtyListing.myProperty.value) {
                return
            }
            user.cash -= realtyListing.myProperty.value
        }
        realtyListing.myProperty.user = user
        user.save()
        realtyListing.myProperty.save()
        realtyListing.delete()
    }

    Loan generateMortgageForProperty(MyProperty myProperty) {
        // TODO: real mortgage generation
        new Loan(
                name: "${myProperty.name} mortgage",
                balance: myProperty.value,
                monthlyPayment: 200,
                loanType: LoanType.MORTGAGE,
                importance: Importance.STANDARD
        )
    }

    public List<RealtyListing> getRealtyListings(User user) {
		RealtyListing.findAllByUser(user)
	}

	void refreshAvailableProperties(User user) {
		List<RealtyListing> realtyListings = getRealtyListings(user);
		replaceRandomProperties(realtyListings);
		ensureTargetPropertyCount(realtyListings);
		RealtyListing.saveAll(realtyListings)
	}

	void ensureTargetPropertyCount(List<RealtyListing> realtyListings) {
		int variance = random.nextInt(MAX_LISTING_VARIANCE) + 1;
		variance *= (random.nextBoolean() ? -1 : 1);
		int newTarget = TARGET_PROPERTY_LISTINGS + variance;

		while (realtyListings.size() != newTarget) {
			if (realtyListings.size() < newTarget) {
				MyProperty property = generateRandomMyProperty()
				RealtyListing realtyListing = new RealtyListing(myProperty: property, user: getUser())
				realtyListings.add(realtyListing);
			} else {
				realtyListings.remove(0).delete();
			}
		}
	}

	void replaceRandomProperties(List<RealtyListing> realtyListings) {
		realtyListings.each {
			if (Math.random() < REFRESH_CHANCE) {
				it.myProperty.properties = generateRandomMyProperty();
				it.save()
			}
		}
	}

	private static MyProperty generateRandomMyProperty() {
		MyProperty property = new MyProperty()
		property.name = RandomStringUtils.randomAscii(10)
		property.numOfUnits = RandomUtils.nextInt(4) + 1
		property.propertyType = PropertyType.RESIDENTIAL
		property.quality = Quality.STANDARD
		property.value = 100
		property.save()
		return property
	}

    public void lease(long listingId) {
        RenterListing renterListing = RenterListing.get(listingId)
        if (renterListing.myProperty.numOfUnits <=
                Renter.findAllByMyProperty(renterListing.myProperty).size()) {
            return
        }
        renterListing.renter.myProperty = renterListing.myProperty
        renterListing.renter.save()
        renterListing.delete()
    }

	public List<Renter> getRenters(User user) {
        Renter.findAllByMyPropertyInList(getOwnedProperties(user))
    }

    public List<Renter> getRentersInProperty(MyProperty myProperty) {
		Renter.findAllByMyProperty(myProperty)
    }

    public int getRentalIncome(User user) {
        int total = 0
        getRenters(user).each {
            total += it.monthlyPayment
        }
        total
    }

    public int getNumOfRenters(User user) {
        return getRenters(user).size()
    }

    public int getMaxNumOfRenters(User user) {
        int total = 0
        getOwnedProperties(user).each {
            total += it.numOfUnits
        }
        total
    }

    public int getNumOfProperties(User user) {
        return getOwnedProperties(user).size()
    }

	@Override
	void newTurn(User user) {
		refreshAvailableProperties(user)
	}
}
