package com.emathias.millionaireave

import com.emathias.millionaireave.exceptions.InsufficientFundsException
import com.emathias.millionaireave.exceptions.InvalidAmountException
import grails.transaction.Transactional

@Transactional
class LoanService extends BaseService {

    List<Loan> getLoans(User user) {
        Loan.findAllByUser(user)
    }

    int getLoanOutgo(User user) {
        int total = 0;
        getLoans(user).each {
            total -= it.getMonthlyPayment();
        }
        return total;
    }

    int getLoansBalance(User user) {
        int total = 0;
        getLoans(user).each {
            total -= it.balance;
        }
        return total;
    }

    void applyForLoan(int amount, User user) {
        Loan loan = new Loan(
                user: user,
                name: "bank loan",
                balance: amount,
                downPayment: 0,
                monthlyPayment: amount / 10,
                loanType: LoanType.BANK_LOAN,
                importance: Importance.STANDARD
        ).save()
    }

    void makeLoanPayment(Loan loan, int amount) {
        if (loan.balance < amount) {
            throw new InvalidAmountException("")
        }
        if (amount <= 0) {
            throw new InvalidAmountException("")
        }
        if (loan.user.cash < amount) {
            throw new InsufficientFundsException("")
        }
        loan.balance -= amount
        loan.user.cash -=amount
        loan.save()
        loan.user.save()
    }

	void makeMonthlyLoanPayment(Loan loan) {
		if (loan.monthlyPayment < loan.balance) {
			makeLoanPayment(loan, loan.monthlyPayment)
		} else {
			makeLoanPayment(loan, loan.balance)
		}
	}

	void makeAllMonthlyLoanPayments(User user) {
		Loan.findAllByUser(user).each {
			makeMonthlyLoanPayment(it)
		}
	}

    def getLoan(long loanId) {
        Loan.get(loanId)
    }
}
