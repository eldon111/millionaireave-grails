class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "main")
		"/endturn"(controller: "main", action: "endturn")
        "500"(view:'main/error')

		"/login/auth" {
			controller = 'openId'
			action = 'auth'
		}
		"/login/openIdCreateAccount" {
			controller = 'openId'
			action = 'createAccount'
		}
	}
}
