package com.emathias.millionaireave

class ApplicationFilters {

	UserService userService

    def filters = {
        all(controller:'*', action:'*') {
            before = {
            }
            after = { Map model ->
				if (model) {
					model['playerName'] = userService.playerFullName
					model['playerCash'] = userService.cash
					model['playerAge'] = userService.ageString
					model['playerCashflow'] = userService.monthlyCashflow
				}
            }
            afterView = { Exception e ->
            }
        }
    }
}
