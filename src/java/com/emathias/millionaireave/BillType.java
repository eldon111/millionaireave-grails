package com.emathias.millionaireave;

public enum BillType {
    UTILITY, SERVICE, MEDICAL
}
