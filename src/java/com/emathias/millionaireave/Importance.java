package com.emathias.millionaireave;

public enum Importance {
	NONE("None", "Who cares?"),
	MEH("Meh", "No biggie"),
	STANDARD("Standard", "Ho-hum"),
	URGENT("Urgent", "Be careful..."),
	CRITICAL("Critical", "Action will be taken");

	public final String name;
	public final String description;

	Importance(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Importance nextLevel() {
		for (Importance value : Importance.values()) {
			if (this.compareTo(value) == -1) {
				return value;
			}
		}
		return this;
	}
}
