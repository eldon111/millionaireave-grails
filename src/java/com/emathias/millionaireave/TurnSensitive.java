package com.emathias.millionaireave;

public interface TurnSensitive {

	public void newTurn(User user);
}
