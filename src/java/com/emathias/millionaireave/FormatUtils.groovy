package com.emathias.millionaireave

import java.text.DecimalFormat

class FormatUtils {

	private static DecimalFormat ucur = new DecimalFormat("\$#,##0;- \$#")
	private static DecimalFormat scur = new DecimalFormat("+ \$#,##0;- \$#")

	static String currencyString(amount) {
		if (!amount) { return "\$0" }
		return ucur.format(amount.toLong())
	}

	static String signedCurrencyString(amount) {
		if (!amount?.toLong()) { return "\$0" }
		return scur.format(amount.toLong())
	}
}
