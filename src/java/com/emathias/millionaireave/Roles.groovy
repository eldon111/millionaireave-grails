package com.emathias.millionaireave

public class Roles {
    public static final String ANONYMOUS_USER = "ROLE_USER";
	public static final String PERSISTED_USER = "ROLE_PERSIST";
	public static final String ADMIN_USER = "ROLE_ADMIN";
}
