package com.emathias.millionaireave;

public enum LoanType {
    MORTGAGE, CREDIT_CARD, BANK_LOAN
}