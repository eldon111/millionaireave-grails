package com.emathias.millionaireave.exceptions;

public class ObjectCacheException extends Exception {
	private static final long serialVersionUID = 3456086712676746617L;

	public ObjectCacheException(String message) {
		super(message);
	}

	public ObjectCacheException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
