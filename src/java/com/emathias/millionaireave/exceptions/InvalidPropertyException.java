package com.emathias.millionaireave.exceptions;

public class InvalidPropertyException extends Exception {
	private static final long serialVersionUID = 3456086151676746617L;

	public InvalidPropertyException(String message) {
		super(message);
	}

	public InvalidPropertyException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
