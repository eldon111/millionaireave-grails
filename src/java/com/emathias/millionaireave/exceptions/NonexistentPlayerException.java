package com.emathias.millionaireave.exceptions;

public class NonexistentPlayerException extends Exception {
	private static final long serialVersionUID = 5700918296472634024L;

	public NonexistentPlayerException(String message) {
		super(message);
	}

	public NonexistentPlayerException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
