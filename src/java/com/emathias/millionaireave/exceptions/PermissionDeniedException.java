package com.emathias.millionaireave.exceptions;

public class PermissionDeniedException extends Exception {
	private static final long serialVersionUID = 179914298541672319L;

	public PermissionDeniedException(String message) {
		super(message);
	}

	public PermissionDeniedException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
