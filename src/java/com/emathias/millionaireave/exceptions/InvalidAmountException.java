package com.emathias.millionaireave.exceptions;

public class InvalidAmountException extends Exception {
	private static final long serialVersionUID = 3456086151676746617L;

	public InvalidAmountException(String message) {
		super(message);
	}

	public InvalidAmountException(String message, Throwable throwable) {
		super(message, throwable);
	}
}

