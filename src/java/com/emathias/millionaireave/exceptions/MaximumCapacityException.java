package com.emathias.millionaireave.exceptions;

public class MaximumCapacityException extends Exception {
	private static final long serialVersionUID = 2380525064186444044L;

	public MaximumCapacityException(String message) {
		super(message);
	}

	public MaximumCapacityException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
