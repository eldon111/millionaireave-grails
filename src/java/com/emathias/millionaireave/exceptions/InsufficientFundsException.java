package com.emathias.millionaireave.exceptions;

public class InsufficientFundsException extends Exception {
	private static final long serialVersionUID = 899302889498341836L;

	public InsufficientFundsException(String message) {
		super(message);
	}

	public InsufficientFundsException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
