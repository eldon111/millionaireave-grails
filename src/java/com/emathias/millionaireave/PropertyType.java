package com.emathias.millionaireave;

public enum PropertyType {
    RESIDENTIAL("Residential", ""),
	COMMERCIAL("Commercial", "");

	public final String name;
	public final String description;

	PropertyType(String name, String description) {
		this.name = name;
		this.description = description;
	}
}