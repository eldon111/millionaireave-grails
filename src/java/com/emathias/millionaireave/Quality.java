package com.emathias.millionaireave;

public enum Quality {
    CONDEMNED("Condemned", "No one can live here!"),
    DILAPIDATED("Dilapidated", "A hunk of junk"),
    RUSTIC("Rustic", "Needs a little work"),
    STANDARD("Standard", "Ho-hum"),
    PLUSH("Plush", "Now we're talking!"),
    EXTRAVAGANT("Extravagant", "Diamond crusted");

    public final String name;
    public final String description;

    Quality(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Quality nextLevel() {
        for (Quality value : Quality.values()) {
            if (this.compareTo(value) == -1) {
                return value;
            }
        }
        return this;
    }
}